
# recylerview-expandable

#### 项目介绍
- 项目名称：recylerview-expandable
- 所属系列：openharmony的第三方组件适配移植
- 功能：基于NestedScrollView+DirectionalLayout封装的可扩展的列表
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 0.1

#### 效果演示

![](demo.gif)
#### 安装教程

1.在项目根目录下的build.gradle文件中，

 ```

allprojects {

    repositories {

        maven {

            url 'https://s01.oss.sonatype.org/content/repositories/releases/'

        }

    }

}

 ```

2.在entry模块的build.gradle文件中，

 ```

 dependencies {

    implementation('com.gitee.chinasoft_ohos:recylerview-expandable:1.0.0')

    ......  
        
 }

 ```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行

如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，

并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下
#### 使用说明
创建单元视图
```xml
  <com.hendraanggrian.recyclerview.widget.ExpandableItem
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:bottom_margin="8vp"
        ohos:left_margin="16vp"
        ohos:right_margin="16vp"
        ohos:top_margin="8vp"
        app:duration="200"
        app:layoutContent="$layout:view_content"
        app:layoutHeader="$layout:view_header"
        ohos:background_element="#FFFFFFFF"
        />
```

创建Adapter, 必须继承 `com.hendraanggrian.recyclerview.widget.Adapter`:
```java
public class ItemAdapter extends Adapter<Item> {

    private LayoutScatter mLayoutScatter;

    public ItemAdapter(Context context, List<Item> data) {
        super(context, data);
     ...
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
       ... 
    }
```

在需要的页面添加列表视图
```xml
<com.hendraanggrian.recyclerview.widget.ExpandableRecyclerView
        ohos:height="match_parent"
        ohos:top_padding="8vp"
        ohos:bottom_padding="8vp"
        ohos:background_element="#FFeeeeee"
        ohos:width="match_parent"/>
```

设置数据:
```java
ExpandableRecyclerView expandableRecyclerView = (ExpandableRecyclerView) findComponentById(ResourceTable.Id_xxx);
ItemAdapter itemAdapter = new ItemAdapter(this, list);
expandableRecyclerView.setAdapter(itemAdapter);
```

#### 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代
- 1.0.0
- 0.0.1-SNAPSHOT

#### 版权和许可信息
Apache License Version 2.0, January 2004
http://www.apache.org/licenses/
TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION
                                 