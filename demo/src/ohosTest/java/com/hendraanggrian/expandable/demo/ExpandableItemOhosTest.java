package com.hendraanggrian.expandable.demo;

import com.hendraanggrian.recyclerview.widget.ExpandableItem;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * ExpandableItem测试
 */
public class ExpandableItemOhosTest {
    private ExpandableItem expandableItem;
    private Context context;

    /**
     * 初始化
     * @throws Exception
     */
    @Before
    public void setUp() {
        context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
        expandableItem = new ExpandableItem(context);
    }

    /**
     * 测试
     */
    @Test
    public void testSetAndGetDuration() {
        int duration = 1000;
        expandableItem.setDuration(duration);
        int result = expandableItem.getDuration();
        Assert.assertEquals(duration, result);
    }
}