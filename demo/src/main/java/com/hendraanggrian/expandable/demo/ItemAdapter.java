package com.hendraanggrian.expandable.demo;

import com.hendraanggrian.expandable.ResourceTable;
import com.hendraanggrian.recyclerview.widget.Adapter;

import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

import java.util.List;

/**
 * ItemAdapter. 需继承Adapter
 */
public class ItemAdapter extends Adapter<Item> {
    private LayoutScatter mLayoutScatter;
    private Context mContext;

    public ItemAdapter(Context context, List<Item> data) {
        super(context, data);
        mLayoutScatter = LayoutScatter.getInstance(context);
        mContext = context;
    }

    @Override
    public Component getComponent(int position, Component cpt, ComponentContainer componentContainer) {
        Item item = getItem(position);
        Component component = mLayoutScatter.parse(ResourceTable.Layout_view_row, componentContainer, false);
        Image imageView = (Image) component.findComponentById(ResourceTable.Id_imageView);
        Text textView = (Text) component.findComponentById(ResourceTable.Id_textView);
        Button button = (Button) component.findComponentById(ResourceTable.Id_button);
        Text num = (Text) component.findComponentById(ResourceTable.Id_num);
        num.setText("Number 10");
        imageView.setPixelMap(item.getDrawable());
        textView.setText(item.getTitle());
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ToastDialog dialog = new ToastDialog(mContext);
                dialog.setText("Clicked！");
                dialog.setDuration(1500);
                dialog.setSize(300, 100);
                dialog.setOffset(0, 200);
                dialog.show();
            }
        });
        return component;
    }


}

