package com.hendraanggrian.expandable.demo;

import com.hendraanggrian.expandable.ResourceTable;
import com.hendraanggrian.recyclerview.widget.ExpandableRecyclerView;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

import java.util.ArrayList;
import java.util.List;

/**
 * 主页
 */
public class MainAbility extends Ability {
    private final List<Item> list = new ArrayList<>();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.getIntColor("#FF747474"));
        setData();
        ExpandableRecyclerView expandableRecyclerView =
                (ExpandableRecyclerView) findComponentById(ResourceTable.Id_esv);
        ItemAdapter itemAdapter = new ItemAdapter(this, list);
        expandableRecyclerView.setAdapter(itemAdapter);
    }


    private void setData() {
        list.add(new Item(ResourceTable.Media_ic_test1, "14 Easy Weekend Getaways"));
        list.add(new Item(ResourceTable.Media_ic_test2, "Why We Travel"));
        list.add(new Item(ResourceTable.Media_ic_test3, "A Paris Farewell"));
        list.add(new Item(ResourceTable.Media_ic_test1, "14 Easy Weekend Getaways"));
        list.add(new Item(ResourceTable.Media_ic_test2, "Why We Travel"));
        list.add(new Item(ResourceTable.Media_ic_test3, "A Paris Farewell"));
        list.add(new Item(ResourceTable.Media_ic_test1, "14 Easy Weekend Getaways"));
        list.add(new Item(ResourceTable.Media_ic_test2, "Why We Travel"));
        list.add(new Item(ResourceTable.Media_ic_test3, "A Paris Farewell"));
        list.add(new Item(ResourceTable.Media_ic_test1, "14 Easy Weekend Getaways"));
        list.add(new Item(ResourceTable.Media_ic_test2, "Why We Travel"));
        list.add(new Item(ResourceTable.Media_ic_test3, "A Paris Farewell"));
        list.add(new Item(ResourceTable.Media_ic_test1, "14 Easy Weekend Getaways"));
        list.add(new Item(ResourceTable.Media_ic_test2, "Why We Travel"));
        list.add(new Item(ResourceTable.Media_ic_test3, "A Paris Farewell"));
        list.add(new Item(ResourceTable.Media_ic_test1, "14 Easy Weekend Getaways"));
        list.add(new Item(ResourceTable.Media_ic_test2, "Why We Travel"));
        list.add(new Item(ResourceTable.Media_ic_test3, "A Paris Farewell"));
        list.add(new Item(ResourceTable.Media_ic_test1, "14 Easy Weekend Getaways"));
        list.add(new Item(ResourceTable.Media_ic_test2, "Why We Travel"));
        list.add(new Item(ResourceTable.Media_ic_test3, "A Paris Farewell"));
        list.add(new Item(ResourceTable.Media_ic_test1, "14 Easy Weekend Getaways"));
        list.add(new Item(ResourceTable.Media_ic_test2, "Why We Travel"));
        list.add(new Item(ResourceTable.Media_ic_test3, "A Paris Farewell"));
    }
}
