package com.hendraanggrian.expandable.demo;

/**
 * 演示用数据实体类
 *
 * @since 2021-6-24
 */
public class Item {
    private int drawable;
    private String title;

    /**
     * 构造方法
     */
    public Item() {
    }

    /**
     * 构造方法
     *
     * @param drawable 图片资源ID
     * @param title    标题文字
     */
    public Item(int drawable, String title) {
        this.drawable = drawable;
        this.title = title;
    }

    /**
     * 获取图片资源ID
     *
     * @return drawable资源ID
     */
    public int getDrawable() {
        return drawable;
    }

    /**
     * 设置资源ID
     *
     * @param drawable 资源ID
     */
    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

    /**
     * 获取标题文字
     *
     * @return title 标题文字
     */
    public String getTitle() {
        return title;
    }

    /**
     * 设置标题文字
     *
     * @param title 标题文字
     */
    public void setTitle(String title) {
        this.title = title;
    }
}
