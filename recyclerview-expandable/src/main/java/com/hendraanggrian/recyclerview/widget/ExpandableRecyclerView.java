package com.hendraanggrian.recyclerview.widget;

import com.hendraanggrian.recyclerview.ResourceTable;

import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * ExpandableRecyclerView
 */
public class ExpandableRecyclerView extends NestedScrollView {
    private DirectionalLayout mDlRoot;
    private List<ExpandableItem> mExpandableList = new ArrayList<>();
    private OnItemClickListener onItemClickListener;
    private OnItemLongClickListener onItemLongClickListener;

    public ExpandableRecyclerView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_expandable, this, true);
        mDlRoot = (DirectionalLayout) findComponentById(ResourceTable.Id_dl_root);
    }

    /**
     * OnItemClickListener
     *
     * @param onItemClickListener OnItemClickListener
     */
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    /**
     * 设置OnItemLongClickListener
     *
     * @param onItemLongClickListener OnItemLongClickListener
     */
    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener) {
        this.onItemLongClickListener = onItemLongClickListener;
    }

    public void setAdapter(Adapter adapter) {
        if (adapter == null) {
            throw new IllegalArgumentException("adapter should not be null");
        }
        int size = adapter.getSize();
        Component component = null;
        for (int i = 0; i < size; i++) {
            int finalI = i;
            component = adapter.getComponent(i, component, mDlRoot);
            DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(
                    DirectionalLayout.LayoutConfig.MATCH_PARENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
            ExpandableItem expandableItem = (ExpandableItem) component.findComponentById
                    (ResourceTable.String_expandableItem_id);
            expandableItem.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    setExpand(expandableItem);
                }
            });
            mExpandableList.add(expandableItem);
            component.setClickedListener(new ClickedListener() {
                @Override
                public void onClick(Component component) {
                    if (onItemClickListener != null) {
                        onItemClickListener.itemClick(component, finalI);
                    }
                }
            });
            component.setLongClickedListener(new LongClickedListener() {
                @Override
                public void onLongClicked(Component component) {
                    if (onItemLongClickListener != null) {
                        onItemLongClickListener.itemLongClick(component, finalI);
                    }
                }
            });
            mDlRoot.addComponent(component, layoutConfig);
        }
    }

    private void setExpand(ExpandableItem expandableItem) {
        if (expandableItem.isOpened()) {
            expandableItem.hide();
        } else {
            for (ExpandableItem item : mExpandableList) {
                if (item.isOpened()) {
                    item.hide();
                }
            }
            expandableItem.show();
        }
    }

    /**
     * Item点击接口
     */
    public interface OnItemClickListener {
        void itemClick(Component component, int position);
    }

    /**
     * Item长按点击回调接口
     */
    public interface OnItemLongClickListener {
        void itemLongClick(Component component, int position);
    }

}


