/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.hendraanggrian.recyclerview.widget;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

import java.util.List;

/**
 * 适配器
 * @param <T> 实例泛型
 */
public abstract class Adapter<T> {
    private List<T> mData;

    public Adapter(Context context, List<T> data) {
        this.mData = data;
    }

    /**
     * 设置数据
     *
     * @param data 数据集合
     */
    public void setData(List<T> data) {
        mData.clear();
        mData.addAll(data);
    }

    /**
     * 获取数据
     *
     * @return List<T> 数据集合
     */
    public List<T> getData() {
        return mData;
    }

    /**
     * 获取数据容量
     *
     * @return int size
     */
    public int getSize() {
        return mData == null ? 0 : mData.size();
    }

    /**
     * getItem
     *
     * @param position Item所在位置下标
     * @return T
     */
    public T getItem(int position) {
        return mData.get(position);
    }

    /**
     * getComponent
     *
     * @param position           Item所在位置下标
     * @param component          Item
     * @param componentContainer Item 容器
     * @return Component
     */
    public abstract Component getComponent(int position, Component component, ComponentContainer componentContainer);
}
