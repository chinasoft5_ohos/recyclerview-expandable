/*

 * Copyright (C) 2021 Huawei Device Co., Ltd.

 * Licensed under the Apache License, Version 2.0 (the "License");

 * you may not use this file except in compliance with the License.

 * You may obtain a copy of the License at

 *

 *     http://www.apache.org/licenses/LICENSE-2.0

 *

 * Unless required by applicable law or agreed to in writing, software

 * distributed under the License is distributed on an "AS IS" BASIS,

 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

 * See the License for the specific language governing permissions and

 * limitations under the License.

 */
package com.hendraanggrian.recyclerview.widget;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.TextTool;


/**
 * 自定义属性工具类
 * <p>
 * 注意： 宽高都为 match_content 且无实际内容时构造方法不会调用
 * 使用方法：
 * xxx extends Component
 * 获取自定义属性：
 * String count = AttrUtils.getStringFromAttr(attrSet,"cus_count","0");
 * <p>
 * 属性定义：
 * 布局头中加入  xmlns:hap="http://schemas.huawei.com/apk/res/ohos" 使用hap区分自定义属性与系统属性
 * 即可使用 hap:cus_count="2"  不加直接使用ohos:cus_count="2"
 *
 *
 * <p>
 */
public class AttrUtils {
    /**
     * 获取自定义的String属性
     *
     * @param attrSet      AttrSet
     * @param name         name
     * @param defaultValue 缺省值
     * @return String value
     */
    public static String getStringFromAttr(AttrSet attrSet, String name, String defaultValue) {
        String value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getStringValue();
        }
        return value;
    }

    /**
     * 获取自定义的Dimension属性
     *
     * @param attrSet      AttrSet
     * @param name         name
     * @param defaultValue 缺省值
     * @return Integer value
     */
    public static Integer getDimensionFromAttr(AttrSet attrSet, String name, Integer defaultValue) {
        Integer value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getDimensionValue();
        }
        return value;
    }

    /**
     * 获取自定义的Integer属性
     *
     * @param attrSet      AttrSet
     * @param name         name
     * @param defaultValue 缺省值
     * @return Integer value
     */
    public static Integer getIntegerFromAttr(AttrSet attrSet, String name, Integer defaultValue) {
        Integer value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getIntegerValue();
        }
        return value;
    }

    /**
     * 获取自定义的Float属性
     *
     * @param attrSet      AttrSet
     * @param name         name
     * @param defaultValue 缺省值
     * @return float value
     */
    public static float getFloatFromAttr(AttrSet attrSet, String name, float defaultValue) {
        float value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getFloatValue();
        }
        return value;
    }

    /**
     * 获取自定义的Long属性
     *
     * @param attrSet      AttrSet
     * @param name         name
     * @param defaultValue 缺省值
     * @return Long
     */
    public static Long getLongFromAttr(AttrSet attrSet, String name, Long defaultValue) {
        Long value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getLongValue();
        }
        return value;
    }

    /**
     * 获取自定义的Color属性
     *
     * @param attrSet      AttrSet
     * @param name         name
     * @param defaultValue 缺省值
     * @return int value
     */
    public static int getColorFromAttr(AttrSet attrSet, String name, int defaultValue) {
        int value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getColorValue().getValue();
        }
        return value;
    }

    /**
     * 获取自定义的Boolean属性
     *
     * @param attrSet      AttrSet
     * @param name         name
     * @param defaultValue 缺省值
     * @return int value
     */
    public static boolean getBooleanFromAttr(AttrSet attrSet, String name, boolean defaultValue) {
        boolean value = defaultValue;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            value = attrSet.getAttr(name).get().getBoolValue();
        }
        return value;
    }

    /**
     * 获取Element
     *
     * @param attrSet        AttrSet
     * @param name           name
     * @param defaultElement 缺省值
     * @return Element element
     */
    public static Element getElementFromAttr(AttrSet attrSet, String name, Element defaultElement) {
        Element element = defaultElement;
        if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
            element = attrSet.getAttr(name).get().getElement();
        }
        return element;
    }

    /**
     * 获取资源ID
     *
     * @param attrSet AttrSet
     * @param name    name
     * @param def     缺省值
     * @return int id
     */
    public static int getLayoutIdFormAttr(AttrSet attrSet, String name, int def) {
        int id = def;
        String idStr = getStringFromAttr(attrSet, name, "");
        if (!TextTool.isNullOrEmpty(idStr) && idStr.contains(":")) {
            id = Integer.parseInt(idStr.substring(idStr.indexOf(":") + 1));
        }
        return id;
    }


}
